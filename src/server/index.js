require("dotenv").config({ silent: true });
const express = require("express");
const app = express();
const port = process.env.PRAK_SERVER_PORT || 3500;
var http = require("http");

app.use(express.json());

app.use((req, res, next) => {
  console.log(`URL: ${req.url}`);
  next();
});

app.set("views", __dirname + "/views");
app.set("view engine", "pug");

app.get("/", (req, res) => {
  res.render("index");
});

app.get("/alive", (req, res) => {
  res.json({
    alive: true,
  });
});

var path = require("path");

app.use(express.static(path.resolve("./dist")));

var server = http.createServer(app);

const { Server } = require("socket.io");
const io = new Server(server);
io.on("connection", (socket) => {
  console.log("a user connected");
  socket.on("disconnect", () => {
    console.log("user disconnected");
  });
});

server.listen(port, () => {
  console.log(`PRAK started at port ${port}`);
});

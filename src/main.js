import { createApp } from "vue";
import "./main.css";
import App from "./App.vue";
import { createHead } from "@vueuse/head";
let app = createApp(App);
const head = createHead();
app.use(head);
app.mount("#app");

import { defineConfig } from "vite";
import vue from "@vitejs/plugin-vue";
import eslintPlugin from "vite-plugin-eslint";
import Components from "unplugin-vue-components/vite";
import AutoImport from "unplugin-auto-import/vite";
// https://vitejs.dev/config/
export default defineConfig({
  server: {
    proxy: {
      "/api": {
        target: "http://localhost:3500",
        changeOrigin: true,
        secure: false,
        ws: true,
      },
    },
  },
  plugins: [
    vue(),
    //eslintPlugin({
    // include: "src/**/*",
    //}),
    AutoImport({
      /* options */
    }),
    Components({
      /* options */
    }),
  ],
});

FROM node:16.15.0-alpine

COPY . .

CMD ['npm','run','start']